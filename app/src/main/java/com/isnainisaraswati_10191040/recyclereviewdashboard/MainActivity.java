package com.isnainisaraswati_10191040.recyclereviewdashboard;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

public class MainActivity extends AppCompatActivity {
    RecyclerView rv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        String[] txt= new String[]{
                "account"
        };
        int[] gambar = new int[]{
                R.drawable.account,
        };
        rv = findViewById(R.id.recycleView);
        RVdashboard adapter = new RVdashboard(this, txt, gambar);
        rv.setAdapter(adapter);
        rv.setLayoutManager(new GridLayoutManager(this, 2));
    }
}

