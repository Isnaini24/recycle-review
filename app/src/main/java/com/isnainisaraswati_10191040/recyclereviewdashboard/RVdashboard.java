package com.isnainisaraswati_10191040.recyclereviewdashboard;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
public class RVdashboard extends RecyclerView.Adapter<RVdashboard.ViewHolder>
{
    Context context;
    String[] txt;
    int[] gambar;
    public RVdashboard(Context context, String[] txt, int[] gambar) {
        this.context = context;
        this.txt = txt;
        this.gambar = gambar;
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int
            viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.item, parent, false);
        return new ViewHolder(view);
    }
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position)
    {
        holder.img.setImageResource(gambar[position]);

        holder.tv.setText(txt[position]);
    }
    @Override
    public int getItemCount() {
        return txt.length;
    }
    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView img;
        TextView tv;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            img = itemView.findViewById(R.id.item_iv);
            tv = itemView.findViewById(R.id.item_tv);

        }
    }
}

